package com.venita.week11;

public class Superman extends Human implements Flyable {

    public Superman(String name) {
        super(name);
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takroff.");
        
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
        
    }
    @Override
    public String toString() {
        return "Superman (" + this.getName() + ")";
    }

    
}
