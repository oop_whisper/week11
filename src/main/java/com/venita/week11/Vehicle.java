package com.venita.week11;

public abstract class Vehicle {
    private String name;
    private String engine;
    public Vehicle(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }
    public String getName() {
        return this.name;
    }
    public String getEngine() {
        return this.engine;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEngine(String engine){
        this.engine = engine;
    }
}
