package com.venita.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Bat batman = new Bat("Batman");
        batman.eat();
        batman.sleep();
        batman.takeoff();
        batman.fly();
        batman.landing();
        Cat mamon = new Cat("Mamon");
        mamon.eat();
        mamon.sleep();
        mamon.walk();
        mamon.run();
        mamon.swim();
        Crocodile croc = new Crocodile("Croc");
        croc.sleep();
        croc.eat();
        croc.swim();
        croc.craw();
        Dog lucy = new Dog("Lucy");
        lucy.sleep();
        lucy.eat();
        lucy.walk();
        lucy.run();
        lucy.swim();
        Fish nemo = new Fish("Nemo");
        nemo.sleep();
        nemo.eat();
        nemo.swim();
        Rat hamtaro = new Rat("Hamtaro");
        hamtaro.sleep();
        hamtaro.eat();
        hamtaro.walk();
        hamtaro.run();
        Snake black = new Snake("Black");
        black.sleep();
        black.eat();
        black.craw();

        Flyable[] flyables = { bird1, boeing, clark, batman };

        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = { bird1, clark, man1, mamon, lucy, hamtaro };
        {
            for (int i = 0; i < walkables.length; i++) {
                walkables[i].walk();
                walkables[i].run();
            }
        }
        Swimable[] Swimable = { clark, man1, mamon, croc, lucy, nemo };
        {
            for (int i = 0; i < Swimable.length; i++) {
                Swimable[i].swim();
            }

        }
        Crawable[] Crawables = { croc, black};
        {
            for (int i = 0; i < Crawables.length; i++) {
                Crawables[i].craw();
            }
        }
    }
}
