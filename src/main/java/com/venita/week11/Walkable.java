package com.venita.week11;

public interface Walkable {
    public void walk();
    public void run();
}
