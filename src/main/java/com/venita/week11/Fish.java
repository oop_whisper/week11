package com.venita.week11;

public class Fish extends Animal implements Swimable {
    public Fish(String name) {
        super(name, 2);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");

    }

    @Override
    public String toString() {
        return "Dog(" + this.getName() + ")";
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }
}
