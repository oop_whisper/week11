package com.venita.week11;

public class Snake extends Animal implements Crawable{
    public Snake(String name) {
        super(name, 2);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");

    }
    @Override
    public String toString() {
        return "Bird("+this.getName()+")";
    }
    @Override
    public void craw() {
        System.out.println(this + " craw.");
        
    }
    
}
